<?php
/**
 * Template Name: Dynamic CV
 */
 
get_header(); ?>
<?php nocache_headers(); ?>
<script>
  function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
</script>

<?php
 $style = get_query_var( 'style', ''); 
 ?>
<?php
    if ($style == '') {
        $all_cvs = glob('static/cv/*.{pdf,html,json,txt}', GLOB_BRACE);
        #echo print_r($all_cvs);
        $random_index = rand(0,count($all_cvs) - 1);
        #echo $random_index;
        $retained_cv = $all_cvs[$random_index];
    }
    else {
        $retained_cv = 'static/cv/'.$style;
    }
    # echo 'retained_cv: '.$retained_cv
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="entry-content">
    <iframe src="<?php echo $retained_cv ?>" scrolling="no" frameborder="0" style="position: relative; height: 100vh; width: 100%;" onload="resizeIframe(this)" >
        <p>Your browser does not support iframes.</p>
    </iframe>
</div>
</article>